#include "WebcamReader.h"



WebcamReader::WebcamReader(int cameraId, double fps, int width, int height) :
	_nextId(0),
	_cameraId(cameraId),
	_fps(fps),
	_width(width),
	_height(height),
	_autofocus(true),
	_run(false)
{
	
}

void WebcamReader::run()
{
	// Already running?
	std::lock_guard<std::mutex> run_guard(_run_mutex);
	if (_run)
		return;

	// Start capturing frames on another thread
	_runThread = std::thread(&WebcamReader::runThread, this);
	_run = true;
}

void WebcamReader::stop()
{
	std::lock_guard<std::mutex> guard(_run_mutex);
	_run = false;
}

void WebcamReader::runThread()
{
	_cap.open(_cameraId, cv::CAP_DSHOW);

	if (!_cap.isOpened()) {
		throw new std::exception("OpenCV VideoCapture could not be opened.");
		return;
	}

	// Note: If for some reason the cam is not running at lower FPS than you set, try to set CAP_PROP_FOURCC before FPS.
	//	     According to the internet, CAP_PROP_FOURCC should be set before FPS/WIDTH/HEIGHT, but for me it was the other way around.
	_cap.set(cv::CAP_PROP_FPS, _fps);
	_cap.set(cv::CAP_PROP_FRAME_WIDTH, (double)_width);
	_cap.set(cv::CAP_PROP_FRAME_HEIGHT, (double)_height);
	_cap.set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'));
	_cap.set(cv::CAP_PROP_AUTOFOCUS, _autofocus ? 1.0 : 0.0);
	_cap.set(cv::CAP_PROP_SETTINGS, 1.0);

	// Threadsafety first
	std::unique_lock<std::mutex> run_guard(_run_mutex);
	std::unique_lock<std::mutex> frames_guard(_frames_mutex);
	frames_guard.unlock();

	cv::Mat frame;
	_run = true;
	while (_run) {
		// Allow other threads to modify _run while we read frames
		run_guard.unlock();

		// Read frame and put it into the queue
		if (_cap.read(frame)) {
			std::shared_ptr<FrameData> frameData = std::make_shared<FrameData>(_nextId++, std::chrono::steady_clock::now(), frame);
			frames_guard.lock();
			_frames.push(frameData);
			frames_guard.unlock();
		}

		// Make sure _run variable is in sync
		run_guard.lock();
	}
	_cap.release();
	_run = false;
	run_guard.unlock();
}


void WebcamReader::setAutofocus(bool focus)
{
	_autofocus = focus;
}

bool WebcamReader::frameAvailable()
{
	std::lock_guard<std::mutex> guard(_frames_mutex);
	return _frames.size() > 0;
}

std::shared_ptr<FrameData> WebcamReader::read()
{
	// Start reading frames if not already running
	if (_runThread.joinable()) {
		run();
	}
	std::shared_ptr<FrameData> p = NULL;
	std::unique_lock<std::mutex> frames_guard(_frames_mutex);
	while (_frames.empty()) {
		frames_guard.unlock();
		std::this_thread::sleep_for(std::chrono::nanoseconds(10));
		frames_guard.lock();
	}
	p = _frames.front();
	_frames.pop();
	frames_guard.unlock();
	return p;
}

WebcamReader::~WebcamReader()
{
	if (_run) {
		std::unique_lock<std::mutex> run_guard(_run_mutex);
		_run = false;
		run_guard.unlock();
		_runThread.join();
	}
}


