#pragma once

// C/C++
#include <chrono>
#include <queue>
#include <mutex>
#include <stdio.h>
#include <iostream>

// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>



/// <summary>
/// Holds all relevant data (i.e. a frame id, the captured frame and the corresponding time).
/// </summary>
struct FrameData {

	const std::size_t id;

	const std::chrono::steady_clock::time_point time;

	const cv::Mat image;

	FrameData(std::size_t id, std::chrono::steady_clock::time_point time, cv::Mat image) :
		id(id),
		time(time),
		image(image) {
	}

};

class WebcamReader
{
	int _fps;
	int _cameraId;
	int _width;
	int _height;
	bool _autofocus;
	std::size_t _nextId;
	cv::VideoCapture _cap;

	/// <summary>
	/// True, if the WebcamReader has started capturing frames.
	/// </summary>
	bool _run;
	std::mutex _run_mutex;

	/// <summary>
	/// Queue to buffer frames, so that no frames are lost.
	/// </summary>
	std::queue<std::shared_ptr<FrameData>> _frames;
	std::mutex _frames_mutex;

	/// <summary>
	/// Frames are read from a <see cref="cv::VideoCapture"/> in this thread.
	/// </summary>
	std::thread _runThread;

public:
	/// <summary>
	/// The <see cref="WebcamReader"/> class provides a thread-safe way to asynchronously read frames from a <see cref="cv::VideoCapture"/>.
	/// </summary>
	/// <param name="cameraId">The camera identifier.</param>
	/// <param name="fps">The FPS.</param>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>
	WebcamReader(int cameraId, double fps, int width, int height);

	/// <summary>
	/// Starts a new thread to start capturing frames from the selected camera.
	/// </summary>
	void run();

	/// <summary>
	/// Sets _run to false, so that the _runThread will terminate.
	/// </summary>
	void stop();

	/// <summary>
	/// Sets the autofocus. Needs to be set before the WebcamReader read its first frame, i.e. WebcamReader::run() or WebcamReader::read() was called.
	/// </summary>
	/// <param name="focus">if set to <c>true</c>, the camera will use it's autofocus.</param>
	void setAutofocus(bool focus);

	/// <summary>
	/// Checks if frames are queued up. Does not block, as long as _frames_mutex is not locked by another thread.
	/// </summary>
	/// <returns>True, if frames in queue.</returns>
	bool frameAvailable();

	/// <summary>
	/// Gets the first frame in the queue, or waits until one is available.
	/// </summary>
	/// <returns>shared_ptr<FrameData> containing the image, an imageId and the capture time.</returns>
	std::shared_ptr<FrameData> read();
	
	/// <summary>
	/// Finalizes an instance of the <see cref="WebcamReader"/> class, possibly waiting for the thread _runThread to finish.
	/// </summary>
	~WebcamReader();

private:
	/// <summary>
	/// This is run on a seperate thread and captures camera frames.
	/// Frames are added to the _frames queue to be read from elsewhere using WebcamReader::read().
	/// </summary>
	void runThread();

};

