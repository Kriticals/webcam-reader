// OpenCVAutofocusTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/features2d.hpp>
#include "WebcamReader.h"

using namespace std;
using namespace cv;

int main()
{
    cv::namedWindow("Webcam Raw", cv::WINDOW_NORMAL);
    cv::resizeWindow("Webcam Raw", cv::Size(1920, 1080));
    WebcamReader webcam(0, 30.0, 1920, 1080);
    webcam.setAutofocus(false);
    webcam.run();

    // Create some random colors
    vector<Scalar> colors;
    RNG rng;
    for (int i = 0; i < 100; i++)
    {
        int r = rng.uniform(0, 256);
        int g = rng.uniform(0, 256);
        int b = rng.uniform(0, 256);
        colors.push_back(Scalar(r, g, b));
    }
    shared_ptr<FrameData> old_frame_data;
    Mat old_frame, old_gray;
    vector<Point2f> p0, p1;
    vector<KeyPoint> p0_orb, p1_orb;
    // Take first frame and find corners in it
    old_frame_data = webcam.read();
    old_frame = old_frame_data->image;
    cvtColor(old_frame, old_gray, COLOR_BGR2GRAY);

    Ptr<Feature2D> b = ORB::create();
    b->detect(old_frame, p0_orb);
    for (int i = 0; i < p0_orb.size(); ++i) {
        p0.push_back(p0_orb[i].pt);
    }

    //goodFeaturesToTrack(old_gray, p0, 100, 0.3, 7, Mat(), 7, false, 0.04);
    // Create a mask image for drawing purposes
    Mat mask = Mat::zeros(old_frame.size(), old_frame.type());

    
    char keyPressed = 0;
    std::chrono::steady_clock::time_point last_frame = std::chrono::steady_clock::now();
    while (keyPressed != 27) {
        Mat frame, frame_gray;

        if (webcam.frameAvailable()) {
            std::shared_ptr<FrameData> frameData = webcam.read();
            

            if (frameData != NULL && frameData->image.size().width > 0) {

                frame = frameData->image;
                cvtColor(frame, frame_gray, COLOR_BGR2GRAY);

                // calculate optical flow
                vector<uchar> status;
                vector<float> err;
                TermCriteria criteria = TermCriteria((TermCriteria::COUNT) + (TermCriteria::EPS), 10, 0.03);
                calcOpticalFlowPyrLK(old_gray, frame_gray, p0, p1, status, err, Size(15, 15), 20, criteria);
                vector<Point2f> good_new;
                for (uint i = 0; i < p0.size(); i++)
                {
                    // Select good points
                    if (status[i] == 1) {
                        good_new.push_back(p1[i]);
                        // draw the tracks
                        line(mask, p1[i], p0[i], colors[i % 100], 2);
                        circle(frame, p1[i], 5, colors[i % 100], -1);
                    }
                }
                Mat img;
                add(frame, mask, img);
                imshow("Frame", img);
                int keyboard = waitKey(30);
                if (keyboard == 'q' || keyboard == 27)
                    break;
                // Now update the previous frame and previous points
                old_gray = frame_gray.clone();
                p0 = good_new;

                std::cout << "FPS: " << 1.0 / (std::chrono::duration_cast<std::chrono::milliseconds> ((frameData->time - last_frame)).count() / 1000.0) << std::endl;
                last_frame = frameData->time;
                cv::imshow("Webcam Raw", frameData->image);
            }
        }
        else {
            std::cout << "No frame available yet, I could do something else, like... baking another QUICHE?! :3" << std::endl;
        }
        
        
        keyPressed = cv::waitKey(1);
    }
    std::cout << "Hellou" << std::endl;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
