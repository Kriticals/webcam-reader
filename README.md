A multi-threaded wrapper for the OpenCV VideoCapture class to asynchronously capture frames.
Frames are queued, so that no frames are lost in case of a performance bottleneck.

ToDo: Provide the possibility to drop frames, if this is desired.